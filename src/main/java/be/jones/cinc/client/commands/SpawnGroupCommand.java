package be.jones.cinc.client.commands;

import be.jones.cinc.client.CincClient;

public class SpawnGroupCommand {

  public static void spawnGroup(String criteriaJson) {
    String command = "{\"name\":\"add_group\", \"arguments\" : " + criteriaJson + " }";

    CincClient.getInstance().sendCommand(command);
  }

}
