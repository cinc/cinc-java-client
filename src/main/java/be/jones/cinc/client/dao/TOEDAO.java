package be.jones.cinc.client.dao;

import be.jones.cinc.client.CincClient;
import be.jones.cinc.client.model.Country;
import be.jones.cinc.client.model.UnitTemplate;
import be.jones.cinc.client.model.UnitTemplate.GroupCategory;
import be.jones.cinc.client.model.UnitTemplate.Category;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.NonNull;

public class TOEDAO {

  private static Gson gson = new GsonBuilder()
      .registerTypeAdapter(Country.TOE.class, new TOEDeserializer())
      .create();

  public static Country.TOE getCountryTOE(@NonNull Integer id) {
    String json = CincClient.getInstance().sendCommand("{\"name\":\"get_country_toe\", \"arguments\":{\"id\":" + Integer.toString(id) + "}}");
    return gson.fromJson(json, Country.TOE.class);
  }

  private static class TOEDeserializer implements JsonDeserializer<Country.TOE> {
    public Country.TOE deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
        throws JsonParseException {

      Type mapType = new TypeToken<Map<String, Collection<String>>>() {}.getType();
      Map<String, Collection<String>> data = context.deserialize(json, mapType);

      Map<Category, Collection<UnitTemplate>> toe = new HashMap<>();

      for (Map.Entry<String,Collection<String>> entry : data.entrySet()) {
        String stringKey = entry.getKey();
        Collection<String> stringUnitList = entry.getValue();

        Category key = convertUnitCategoryToEnum(stringKey);
        Collection<UnitTemplate> unitTemplates = createUnitList(key, stringUnitList);

        toe.put(key, unitTemplates);
      }

      return new Country.TOE(toe);
    }


    private Collection<UnitTemplate> createUnitList(Category category, Collection<String> stringUnitList) {
      return stringUnitList.stream().map(nameString -> new UnitTemplate(nameString, category, getGroupCategory(
          category))).collect(Collectors.toList());
    }


    private Category convertUnitCategoryToEnum(String stringGranularCategory) {

      Category category = null;

      switch (stringGranularCategory) {
        case "air_defence":
          category = Category.AIR_DEFENCE;
          break;
        case "armor":
          category = Category.ARMOR;
          break;
        case "artillery":
          category = Category.ARTILLERY;
          break;
        case "fortification":
          category = Category.FORTIFICATION;
          break;
        case "helicopters":
          category = Category.HELICOPTER;
          break;
        case "heliports":
          category = Category.HELIPORT;
          break;
        case "infantry":
          category = Category.INFANTRY;
          break;
        case "planes":
          category = Category.PLANE;
          break;
        case "ships":
          category = Category.SHIP;
          break;
        case "train":
          category = Category.TRAIN;
          break;
        case "unarmed":
          category = Category.UNARMED;
          break;
    }

      return category;
    }

    private GroupCategory getGroupCategory(Category stringCategory) {

      GroupCategory groupCategory = null;

      switch (stringCategory) {
        case AIR_DEFENCE:
          groupCategory = GroupCategory.GROUND_UNIT;
          break;
        case ARMOR:
          groupCategory = GroupCategory.GROUND_UNIT;
          break;
        case ARTILLERY:
          groupCategory = GroupCategory.GROUND_UNIT;
          break;
        case FORTIFICATION:
          groupCategory = GroupCategory.STRUCTURE;
          break;
        case HELICOPTER:
          groupCategory = GroupCategory.HELICOPTER;
          break;
        case HELIPORT:
          groupCategory = GroupCategory.STRUCTURE;
          break;
        case INFANTRY:
          groupCategory = GroupCategory.GROUND_UNIT;
          break;
        case PLANE:
          groupCategory = GroupCategory.AIRPLANE;
          break;
        case SHIP:
          groupCategory = GroupCategory.SHIP;
          break;
        case TRAIN:
          groupCategory = GroupCategory.TRAIN;
          break;
        case UNARMED:
          groupCategory = GroupCategory.GROUND_UNIT;
          break;
      }
      return groupCategory;
    }
  }

}
