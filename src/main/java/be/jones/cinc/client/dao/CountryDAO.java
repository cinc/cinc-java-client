package be.jones.cinc.client.dao;

import be.jones.cinc.client.CincClient;
import be.jones.cinc.client.model.Country;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class CountryDAO {

  private static Gson gson = new GsonBuilder()
      .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
      .create();

  public static Collection<Country> listAll() {
    String json = CincClient.getInstance().sendCommand("{\"name\":\"get_countries\"}");

    List<Country> countries = gson.fromJson(json, new TypeToken<List<Country>>() {
    }.getType());
    Collections.sort(countries);

    return countries;
  }

}
