package be.jones.cinc.client.model;

import be.jones.cinc.client.dao.TOEDAO;
import be.jones.cinc.client.model.UnitTemplate.Category;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import java.util.Collection;
import java.util.Map;
import lombok.Data;
import lombok.Value;

@Data
public class Country implements Comparable {

  private static Gson gson = new GsonBuilder().create();

  @SerializedName("world_id")
  private final Integer id;
  private final Integer coalition;
  private final String name;
  private final String shortName;

  private TOE toe;

  public TOE getToe() {
    if (this.toe == null) {
      toe = TOEDAO.getCountryTOE(this.id);
    }
    return this.toe;
  }

  @Override
  public String toString() {
    return gson.toJson(this);
  }

  @Override
  public int compareTo(Object o) {
    return this.getName().compareTo(((Country) o).getName());
  }

  @Value
  public static class TOE {
    private final Map<Category, Collection<UnitTemplate>> units;
  }

}
