package be.jones.cinc.client.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Value;

@Value
public class UnitTemplate {

  private static Gson gson = new GsonBuilder().create();

  String name;
  Category category;
  GroupCategory groupCategory;

  public enum Category {
    AIR_DEFENCE,
    ARMOR,
    ARTILLERY,
    FORTIFICATION,
    HELICOPTER,
    HELIPORT,
    INFANTRY,
    PLANE,
    SHIP,
    TRAIN,
    UNARMED
  }

  public enum GroupCategory {
    AIRPLANE(0),
    HELICOPTER(1),
    GROUND_UNIT(2),
    SHIP(3),
    STRUCTURE(2), // Structures are ground units in DCS
    TRAIN(4); // Trains however are NOT ground units apparently...

    private int value;

    private GroupCategory(int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }
  }

  @Override
  public String toString() {
    return gson.toJson(this);
  }
}
