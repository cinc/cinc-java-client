package be.jones.cinc.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class CincClient {

  private Socket socket;

  private static CincClient instance = new CincClient();

  /* A private Constructor prevents any other
   * class from instantiating.
   */
  private CincClient() {
  }

  public static CincClient getInstance() {
    return instance;
  }


  public void connect(InetAddress serverAddress, int serverPort) throws Exception {
    this.socket = new Socket(serverAddress, serverPort);
  }

  public void disconnect() {
    try {
      this.socket.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public boolean isConnected() {
    return socket.isConnected();
  }

  public String sendCommand(String request) {

    System.out.println(request);

    String response = null;

    try {
      BufferedReader in =
          new BufferedReader(
              new InputStreamReader(this.socket.getInputStream()));

      PrintWriter out = new PrintWriter(this.socket.getOutputStream(), true);
      out.println(request);
      out.flush();

      response = in.readLine();

      System.out.println(response);

    } catch (IOException e) {
      e.printStackTrace();
    }

    return response;
  }
}

